#!/bin/bash
PROJECTNAME=top
BOARD_FREQ=65
CPU_FREQ=100
FPGA_VARIANT=25k
FPGA_PACKAGE=CABGA256
VERILOGS=$1

pcf_file=io_pro.pcf
yosys -q -DULX3S -DBOARD_FREQ=$BOARD_FREQ -DCPU_FREQ=$CPU_FREQ -p "synth_ecp5 -abc9 -top $PROJECTNAME -json $PROJECTNAME.json" $VERILOGS  || exit
nextpnr-ecp5 --json $PROJECTNAME.json --lpf pro.lpf --textcfg $PROJECTNAME"_out".config --freq $BOARD_FREQ --$FPGA_VARIANT --package $FPGA_PACKAGE --speed 6 || exit
ecppack --compress --svf $PROJECTNAME.svf $PROJECTNAME"_out.config" $PROJECTNAME.bin || exit # --svf-rowsize 100000
