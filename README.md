# Okapi GPU
Okapi is a project to create a simple GPU design.
This is just for fun, so we are only trying to match the performance of an [N64][1] ([5th generation console][2]).
We are developing it on an [iCESugar-Pro][3] FPGA development board using [Amaranth][4].

[1]: https://en.wikipedia.org/wiki/Nintendo_64
[2]: https://en.wikipedia.org/wiki/Fifth_generation_of_video_game_consoles
[3]: http://www.tindie.com/products/johnnywu/icesugar-pro-fpga-development-board
[4]: https://github.com/amaranth-lang/amaranth

Right now, it has VGA output at 640×480 and can show a single static triangle.

## Prerequisites
* Python
* [Amaranth](https://github.com/amaranth-lang/amaranth)
* [`matplotlib`](https://matplotlib.org/) (for testing)
* [`yosys`](https://yosyshq.net/yosys/)
* [`nextpnr-ecp5`](https://github.com/YosysHQ/nextpnr)

## Building
Compile Amaranth to Verilog by running

    python main.py
This will output `main.v`.
If you just need to verify that your Amaranth design is valid, you can stop here.

To generate a `.bin` file that can be loaded onto the iCESugar-Pro, run

    ./run_pro.sh main.v
to generate `top.bin`.
