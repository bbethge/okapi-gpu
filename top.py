from amaranth import *  # type: ignore

from clock import ClockGenerator
from main import Main


class Top(Elaboratable):
    def __init__(self):
        self.gpdi_dp = Signal(4)
        self.gpdi_dn = Signal(4)

    def elaborate(self, _):
        m = Module()
        m.domains.tmds = ClockDomain(local=True)
        m.submodules.clock = clock = ClockGenerator()
        m.submodules.main = main = Main()
        m.d.comb += [
            clock.clock_in_25MHz.eq(ClockSignal('sync')),
            ClockSignal('tmds').eq(clock.clock_250MHz),
            ResetSignal('tmds').eq(ResetSignal('sync')),
            self.gpdi_dp.eq(main.gpdi_dp),
            self.gpdi_dn.eq(main.gpdi_dn),
        ]
        return m


if __name__ == '__main__':
    from amaranth.back import verilog

    system = Top()
    with open('top.v', 'w') as file:
        file.write(verilog.convert(system, ports=[system.gpdi_dp, system.gpdi_dn]))
