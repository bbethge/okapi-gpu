from amaranth import *  # type: ignore
from amaranth.lib.data import *  # type: ignore

from gpu import Gpu
from hdmi import HdmiOutput
from scaler import Scaler


class Main(Elaboratable):
    def __init__(self):
        self.gpdi_dp = Signal(4)
        self.gpdi_dn = Signal(4)

    def elaborate(self, _):
        m = Module()

        command_buffer = [
            3,  # Triangle count
            0x97F,  # Pink
            0x082_0, 0x005_0,  # Vertex 0
            0x02D_0, 0x08B_0,  # Vertex 1
            0x123_0, 0x0E2_0,  # Vertex 2
            0x3E1,   # Green
            0x0A7_0, 0x092_0,  # Vertex 0
            0x130_0, 0x029_0,  # Vertex 1
            0x104_0, 0x008_0,  # Vertex 2
            0x538,  # Dark pink
            0x09F_0, 0x03E_8,  # Vertex 0
            0x012_4, 0x043_9,  # Vertex 1
            0x023_0, 0x0B3_0,  # Vertex 2
        ]
        gpu = Gpu(width=320, height=230, component_width=4, command_buffer_init=command_buffer)
        m.submodules.gpu = gpu

        m.submodules.scaler = scaler = Scaler(input_width=320, input_height=240)
        with m.If(scaler.pixel_address < 320*230):
            m.d.comb += gpu.framebuffer_read_enable.eq(1)
            m.d.comb += gpu.framebuffer_address.eq(scaler.pixel_address)
        with m.Else():
            m.d.comb += gpu.framebuffer_read_enable.eq(0)
            m.d.comb += gpu.framebuffer_address.eq(0)

        pixel = Signal(ArrayLayout(4, 3))
        with m.If((scaler.pixel_address != 0) & (scaler.pixel_address < 320*230 + 1)):
            m.d.comb += pixel.eq(gpu.framebuffer_data)
        with m.Elif(scaler.pixel_address == 0):
            m.d.comb += pixel.eq(0x00F)
        with m.Else():
            m.d.comb += pixel.eq(0)

        m.submodules.hdmi = hdmi = HdmiOutput()
        m.d.comb += scaler.new_frame.eq(hdmi.new_frame)
        m.d.comb += scaler.new_row.eq(hdmi.new_line)
        m.d.comb += scaler.output_ready.eq(hdmi.ready)

        with m.If(hdmi.ready):
            for i in range(3):
                m.d.comb += hdmi.color_in[i].eq(Cat(pixel[i], pixel[i]))
        with m.Else():
            m.d.comb += hdmi.color_in.eq(0)

        for i, output in enumerate(
                [hdmi.color_out[2], hdmi.color_out[1], hdmi.color_out[0], ClockSignal('sync')]
        ):
            diff_output = DifferentialOutput()
            m.submodules += diff_output
            m.d.comb += [
                diff_output.input.eq(output),
                self.gpdi_dp[i].eq(diff_output.output_pos),
                self.gpdi_dn[i].eq(diff_output.output_neg),
            ]

        #hdmi_port = platform.request('HDMI')       
        #m.d.comb += hdmi_port.red.o.eq(hdmi.color_out[2])
        #m.d.comb += hdmi_port.green.o.eq(hdmi.color_out[1])
        #m.d.comb += hdmi_port.blue.o.eq(hdmi.color_out[0])
        #m.d.comb += hdmi_port.clock.o.eq(ClockSignal('sync'))

        return m


class DifferentialOutput(Elaboratable):
    """For unknown reasons, this has to be a separate module."""
    def __init__(self):
        self.input = Signal()
        self.output_pos = Signal()
        self.output_neg = Signal()

    def elaborate(self, _):
        m = Module()
        m.d.comb += [self.output_pos.eq(self.input), self.output_neg.eq(~self.input)]
        return m


if __name__ == '__main__':
    import sys

    from amaranth.back import verilog
    from amaranth.sim import Simulator

    system = Main()
    if sys.argv[1] == 'simulate':
        simulator = Simulator(system)
        simulator.add_clock(4e-8, domain='sync')
        simulator.add_clock(4e-9, domain='tmds')
        with simulator.write_vcd('main_test.vcd'):
            simulator.run_until(4e-6, run_passive=True)
    elif sys.argv[1] == 'generate':
        with open('main.v', 'w') as file:
            file.write(verilog.convert(system, ports=[system.gpdi_dp, system.gpdi_dn]))
