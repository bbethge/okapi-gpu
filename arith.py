import operator

from amaranth import *  # type: ignore
from amaranth.hdl.ast import ValueCastable
from amaranth.lib.data import *  # type: ignore


def twos_complement(a):
    return ((~a) + 1)[0:len(a)]


class FractionalLayout(FlexibleLayout):
    def __init__(self, signed, integer_width, fraction_width):
        super().__init__(
            integer_width + fraction_width,
            {
                'sign':
                    Field(unsigned(1), integer_width + fraction_width - 1)
                    if signed else Field(unsigned(0), integer_width + fraction_width),
                'integer': Field(Shape(signed=signed, width=integer_width), fraction_width),
                'fraction': Field(unsigned(fraction_width), 0),
            }
        )
        self.signed = signed

    def __call__(self, value):
        return Fractional(self, value)


class Fractional(View):
    @ValueCastable.lowermethod
    def as_value(self):
        # We need to override this so that signed fractional numbers
        # will be converted to signed values.  The decorator is used
        # because it’s used in the original implementation, so I guess
        # it’s needed here too.
        value = super().as_value()
        return value.as_signed() if self.shape().signed else value

    def __add__(self, other):  # pyright: ignore [reportIncompatibleMethodOverride]
        return self._generic_add(operator.add, other)

    def __sub__(self, other):
        return self._generic_add(operator.sub, other)

    def _generic_add(self, op, other):
        self_value, other_value = Fractional._as_compatible_values(self, other)
        result_value = op(self_value, other_value)
        fraction_width = max(len(self.fraction), len(other.fraction))
        return \
            Fractional(
                FractionalLayout(
                    signed=self.shape().signed or other.shape().signed,
                    integer_width=len(result_value)-fraction_width,
                    fraction_width=fraction_width,
                ),
                result_value
            )

    def __eq__(self, other):
        return self._generic_compare(operator.eq, other)

    def __ne__(self, other):
        return self._generic_compare(operator.ne, other)

    def __lt__(self, other):
        return self._generic_compare(operator.lt, other)

    def __le__(self, other):
        return self._generic_compare(operator.le, other)

    def __gt__(self, other):
        return self._generic_compare(operator.gt, other)

    def __ge__(self, other):
        return self._generic_compare(operator.ge, other)

    def _generic_compare(self, op, other):
        self_value, other_value = Fractional._as_compatible_values(self, other)
        return op(self_value, other_value)

    @staticmethod
    def _as_compatible_values(a, b):
        a_value = a.as_value() << max(0, len(b.fraction) - len(a.fraction))
        b_value = b.as_value() << max(0, len(a.fraction) - len(b.fraction))
        return a_value, b_value

    def __neg__(self):
        signed_self = self.to_signed()
        return \
            Fractional(
                FractionalLayout(
                    signed=True,
                    integer_width=len(signed_self.integer)+1,
                    fraction_width=len(signed_self.fraction),
                ),
                -signed_self.as_value()
            )

    def to_signed(self):
        if self.shape().signed:
            return self
        else:
            result_value = Cat(self.as_value(), Const(0, shape=1)).as_signed()
            return \
                Fractional(
                    FractionalLayout(
                        signed=True,
                        integer_width=len(result_value)-len(self.fraction),
                        fraction_width=len(self.fraction)
                    ),
                    result_value
                )

    def __abs__(self):
        return Mux(self.sign, -self, self)

    def __mul__(self, other):
        result_value = self.as_value() * other.as_value()
        fraction_width = len(self.fraction) + len(other.fraction)
        return \
            Fractional(
                FractionalLayout(
                    signed=self.shape().signed or other.shape().signed,
                    integer_width=len(result_value)-fraction_width,
                    fraction_width=fraction_width
                ),
                result_value
            )

    def truncate_to(self, fraction_width):
        assert fraction_width <= len(self.fraction)
        result_value = self.as_value().shift_right(len(self.fraction) - fraction_width)
        return \
            Fractional(
                FractionalLayout(
                    signed=self.shape().signed,
                    integer_width=len(self.integer),
                    fraction_width=fraction_width,
                ),
                result_value
            )

    def floor(self):
        return self.truncate_to(fraction_width=0)

    def ceil(self):
        just_under_1 = Fractional(self.shape(), (1 << len(self.fraction)) - 1)
        return (self + just_under_1).floor()

    def mod1(self):
        return \
            Fractional(
                FractionalLayout(
                    signed=False,
                    integer_width=0,
                    fraction_width=len(self.fraction),
                ),
                self.fraction
            )

    @staticmethod
    def zero():
        return Fractional(FractionalLayout(False, 1, 0), 0)

    @staticmethod
    def half():
        return Fractional(FractionalLayout(False, 0, 1), 1)

    @staticmethod
    def one():
        return Fractional(FractionalLayout(False, 1, 0), 1)


if __name__ == '__main__':
    from amaranth.sim import *  # type: ignore


    def test_binary_operator(op, x_layout, x, y_layout, y, result_shape, result):
        system = Module()
        x_signal = Signal(FractionalLayout(*x_layout))
        y_signal = Signal(FractionalLayout(*y_layout))
        result_signal = Signal(result_shape)
        system.d.comb += result_signal.eq(op(x_signal, y_signal))

        def bench():
            yield x_signal.eq(x)
            yield y_signal.eq(y)
            yield Settle()
            if hasattr(result_signal, 'as_value'):
                actual_result = yield result_signal.as_value()
            else:
                actual_result = yield result_signal
            assert actual_result == result

        simulator = Simulator(system)
        simulator.add_process(bench)
        simulator.run()


    test_binary_operator(
        operator.add,
        x_layout=(False, 12, 8),
        x=0x912_34,
        y_layout=(False, 4, 12),
        y=0x5_ABC,
        result_shape=FractionalLayout(False, 13, 12),
        result=0x917_DFC,
    )

    test_binary_operator(
        operator.add,
        x_layout=(True, 8, 4),
        x=-0x72_A,
        y_layout=(True, 8, 8),
        y=0x6F_31,
        result_shape=FractionalLayout(True, 9, 8),
        result=-0x03_6F
    )

    test_binary_operator(
        operator.lt,
        x_layout=(False, 12, 4),
        x=0x0BC_4,
        y_layout=(False, 8, 12),
        y=0xD0_123,
        result_shape=unsigned(1),
        result=1,
    )
