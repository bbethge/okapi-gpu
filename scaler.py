from amaranth import *  # type: ignore


class Scaler(Elaboratable):
    def __init__(self, input_width, input_height):
        self.input_width = input_width
        self.input_height = input_height

        # Inputs
        self.new_row = Signal()
        self.new_frame = Signal()
        self.output_ready = Signal()
        
        # Outputs
        # The address of the pixel to be displayed on the next cycle.
        # This is good for addressing synchronous memories.
        self.pixel_address = Signal(range(input_width * input_height))

    def elaborate(self, _):
        m = Module()

        # The current row of the output, or 2*self.input_height - 1 if
        # in vertical blank.
        output_row = Signal(range(2 * self.input_height))
        with m.If(self.new_frame):
            m.d.sync += output_row.eq(0)
        with m.Elif(self.new_row & (output_row != 2*self.input_height - 1)):
            m.d.sync += output_row.eq(output_row + 1)

        # The current column of the output, or 2*self.input_width - 1 if
        # in horizontal blank.  (Still counts columns during vertical
        # blank.)
        output_column = Signal(range(2 * self.input_width))
        with m.If(self.new_row):
            m.d.sync += output_column.eq(0)
        with m.Elif(output_column != 2*self.input_width - 1):
            m.d.sync += output_column.eq(output_column + 1)

        with m.If(~output_column[0]):
            # Next column is odd numbered; we need to update the pixel
            # address so it is ready before the next even-numbered
            # pixel.
            with m.If(output_column == 2*self.input_width - 2):
                # End of row coming up; need to calculate the first
                # pixel of the next row.
                with m.If(~output_row[0]):
                    # We are on an even numbered output row, so go back
                    # to the start of the input row.
                    m.d.sync += self.pixel_address.eq(self.pixel_address - (self.input_width-1))
                with m.Elif(output_row < 2*self.input_height - 1):
                    # Proceed to the next row.
                    m.d.sync += self.pixel_address.eq(self.pixel_address + 1)
                with m.Elif(self.output_ready):
                    # End of last row; prepare for next frame.
                    m.d.sync += self.pixel_address.eq(0)
            with m.Elif(self.output_ready):
                m.d.sync += self.pixel_address.eq(self.pixel_address + 1)

        return m


if __name__ == '__main__':
    from amaranth.sim import Simulator

    m = Module()
    m.submodules.scaler = scaler = Scaler(320, 240)

    column = Signal(range(800))
    with m.If(column == 799):
        m.d.sync += column.eq(0)
    with m.Else():
        m.d.sync += column.eq(column + 1)

    row = Signal(range(525))
    with m.If(column == 799):
        with m.If(row == 524):
            m.d.sync += row.eq(0)
        with m.Else():
            m.d.sync += row.eq(row + 1)

    m.d.comb += scaler.new_row.eq(column == 799)
    m.d.comb += scaler.new_frame.eq((column == 799) & (row == 524))
    m.d.comb += scaler.output_ready.eq((column < 640) & (row < 480))

    simulator = Simulator(m)
    simulator.add_clock(40e-9)
    with simulator.write_vcd('scaler_test.vcd'):
        simulator.run_until(1/59, run_passive=True)
