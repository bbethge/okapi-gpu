from amaranth import *  # type: ignore
from amaranth.lib.data import *  # type: ignore

from arith import (
    Fractional,
    FractionalLayout,
)


SCREEN_COORDINATE_SHAPE = FractionalLayout(signed=True, integer_width=12, fraction_width=4)
SCREEN_COORDINATE_DELTA_SHAPE = \
    FractionalLayout(
        signed=True,
        integer_width=SCREEN_COORDINATE_SHAPE['integer'].width+1,
        fraction_width=SCREEN_COORDINATE_SHAPE['fraction'].width,
    )
TRIANGLE_SHAPE = \
    StructLayout({
        'color': ArrayLayout(4, 3),
        'vertices': ArrayLayout(ArrayLayout(SCREEN_COORDINATE_SHAPE, 2), 3),
    })
EDGE_GENERATOR_ERROR_SHAPE = \
    FractionalLayout(
        signed=True,
        integer_width=SCREEN_COORDINATE_DELTA_SHAPE['integer'].width+2,
        fraction_width=2*SCREEN_COORDINATE_DELTA_SHAPE['fraction'].width,
    )
PIXEL_CENTER_COORDINATE_SHAPE = \
    FractionalLayout(
        signed=True, integer_width=SCREEN_COORDINATE_SHAPE['integer'].width, fraction_width=1
    )
WORDS_PER_TRIANGLE = 7

zero = Fractional.zero
half = Fractional.half
one = Fractional.one


class Gpu(Elaboratable):
    def __init__(self, width, height, component_width, command_buffer_init):
        # Parameters
        self.width = width
        self.height = height
        self.component_width = component_width
        self.command_buffer_init = command_buffer_init
        self.command_buffer_size = len(command_buffer_init)

        # Inputs
        self.framebuffer_address = Signal(range(self.width * self.height))
        self.framebuffer_read_enable = Signal(1)

        # Outputs
        self.framebuffer_data = Signal(3 * self.component_width)
        self.done = Signal(1)

        self._framebuffer = Memory(width=3*self.component_width, depth=self.width*self.height)

    @property
    def framebuffer(self):
        """Simulation only."""
        return self._framebuffer

    def elaborate(self, _):
        m = Module()

        framebuffer_read_port = self._framebuffer.read_port(transparent=False)
        framebuffer_write_port = self._framebuffer.write_port()
        m.submodules.framebuffer = self._framebuffer
        m.d.comb += framebuffer_read_port.addr.eq(self.framebuffer_address)
        m.d.comb += framebuffer_read_port.en.eq(self.framebuffer_read_enable)
        m.d.comb += self.framebuffer_data.eq(framebuffer_read_port.data)

        command_buffer = \
            Memory(
                width=SCREEN_COORDINATE_SHAPE.size,
                depth=self.command_buffer_size,
                init=self.command_buffer_init,
            )
        command_read_port = command_buffer.read_port()
        m.submodules.command_buffer = command_buffer

        triangle_count = Signal(command_buffer.width)
        triangle_index = Signal(range(self.command_buffer_size // WORDS_PER_TRIANGLE))
        m.d.comb += command_read_port.en.eq(1)

        vertex_index = Signal(range(6))
        vertex_words = Signal(ArrayLayout(SCREEN_COORDINATE_SHAPE, 6))
        vertices = Signal(ArrayLayout(ArrayLayout(SCREEN_COORDINATE_SHAPE, 2), 3))
        m.d.comb += vertices.eq(vertex_words)

        vertex_orderer = VertexOrderer()
        m.submodules.vertex_orderer = vertex_orderer
        m.d.comb += vertex_orderer.input.eq(vertices)

        pixel_x = Signal(range(self.width))
        pixel_y = Signal(range(self.height))
        # We either have to multiply here or do an extra multiplication in the
        # initial value calculator.  Doing it here makes more sense.
        m.d.comb += framebuffer_write_port.addr.eq(self.width*pixel_y + pixel_x)
        m.d.comb += framebuffer_write_port.en.eq(0)

        left_edge_generator = EdgeGenerator()
        right_edge_generator = EdgeGenerator()
        m.submodules.left_edge_generator = left_edge_generator
        m.submodules.right_edge_generator = right_edge_generator
        end_pixel_x = Signal(range(self.width))
        left_edge_points = Signal(ArrayLayout(point_layout(SCREEN_COORDINATE_SHAPE), 2))
        right_edge_points = Signal(ArrayLayout(point_layout(SCREEN_COORDINATE_SHAPE), 2))
        m.d.comb += [
            left_edge_generator.start.eq(left_edge_points[0]),
            left_edge_generator.end.eq(left_edge_points[1]),
            right_edge_generator.start.eq(right_edge_points[0]),
            right_edge_generator.end.eq(right_edge_points[1]),
            pixel_y.eq(left_edge_generator.y.truncate_to(fraction_width=0)),
            self.done.eq(0)
        ]

        with m.FSM():
            with m.State('Begin'):
                m.d.comb += command_read_port.en.eq(1)
                m.d.sync += command_read_port.addr.eq(1)
                m.next = 'Load Triangle Count'
            with m.State('Load Triangle Count'):
                m.d.comb += command_read_port.en.eq(1)
                m.d.sync += command_read_port.addr.eq(command_read_port.addr + 1)
                m.d.sync += triangle_count.eq(command_read_port.data)
                m.d.sync += triangle_index.eq(0)
                m.next = 'Load Color'
            with m.State('Load Color'):
                m.d.comb += command_read_port.en.eq(1)
                m.d.sync += framebuffer_write_port.data.eq(command_read_port.data)
                m.d.sync += command_read_port.addr.eq(command_read_port.addr + 1)
                m.d.sync += vertex_index.eq(0)
                m.next = 'Load Vertices'
            with m.State('Load Vertices'):
                m.d.comb += command_read_port.en.eq(1)
                m.d.sync += vertex_words[5].eq(command_read_port.data)
                for i in range(0, 5):
                    m.d.sync += vertex_words[i].eq(vertex_words[i+1])
                with m.If(vertex_index == 5):
                    m.d.sync += vertex_index.eq(0)
                    m.next = 'Sort Vertices'
                with m.Else():
                    m.d.sync += vertex_index.eq(vertex_index + 1)
                    m.d.sync += command_read_port.addr.eq(command_read_port.addr + 1)
            with m.State('Sort Vertices'):
                m.d.sync += [
                    left_edge_points[0].eq(vertex_orderer.output[0]),
                    left_edge_points[1].eq(vertex_orderer.output[1]),
                    right_edge_points[0].eq(vertex_orderer.output[0]),
                    right_edge_points[1].eq(vertex_orderer.output[2]),
                ]
                m.next = 'Initialize Edge Generators'
            with m.State('Initialize Edge Generators'):
                m.d.comb += left_edge_generator.initialize.eq(1)
                m.d.comb += right_edge_generator.initialize.eq(1)
                m.next = 'Request First Row'
            with m.State('Request First Row'):
                m.d.comb += left_edge_generator.request_next.eq(1)
                m.d.comb += right_edge_generator.request_next.eq(1)
                m.next = 'Start Row'
            with m.State('Start Row'):
                with m.If(~(left_edge_generator.valid | left_edge_generator.done)):
                    m.d.comb += left_edge_generator.request_next.eq(1)
                
                with m.If(~(right_edge_generator.valid | right_edge_generator.done)):
                    m.d.comb += right_edge_generator.request_next.eq(1)

                with m.If(left_edge_generator.valid & right_edge_generator.valid):
                    m.d.sync += pixel_x.eq(left_edge_generator.x.integer)
                    m.d.sync += end_pixel_x.eq(right_edge_generator.x.integer)
                    m.next = 'Draw'
                with m.Elif(left_edge_generator.done & right_edge_generator.done):
                    m.next = 'Triangle Done'
                with m.Elif(left_edge_generator.done & right_edge_generator.valid):
                    m.next = 'Reinitialize Left Edge Generator'
                    m.d.sync += left_edge_points[0].eq(left_edge_points[1])
                    m.d.sync += left_edge_points[1].eq(right_edge_points[1])
                with m.Elif(right_edge_generator.done & left_edge_generator.valid):
                    m.next = 'Reinitialize Right Edge Generator'
                    m.d.sync += right_edge_points[0].eq(right_edge_points[1])
                    m.d.sync += right_edge_points[1].eq(left_edge_points[1])
            with m.State('Draw'):
                with m.If(pixel_x < end_pixel_x):
                    m.d.comb += framebuffer_write_port.en.eq(1)
                    m.d.sync += pixel_x.eq(pixel_x + 1)
                with m.Else():
                    m.d.comb += left_edge_generator.request_next.eq(1)
                    m.d.comb += right_edge_generator.request_next.eq(1)
                    m.next = 'Start Row'
            with m.State('Reinitialize Left Edge Generator'):
                m.d.comb += left_edge_generator.initialize.eq(1)
                m.next = 'Request Next From Left'
            with m.State('Request Next From Left'):
                m.d.comb += left_edge_generator.request_next.eq(1)
                m.next = 'Start Row'
            with m.State('Reinitialize Right Edge Generator'):
                m.d.comb += right_edge_generator.initialize.eq(1)
                m.next = 'Request Next From Right'
            with m.State('Request Next From Right'):
                m.d.comb += right_edge_generator.request_next.eq(1)
                m.next = 'Start Row'
            with m.State('Triangle Done'):
                with m.If(triangle_index + 1 == triangle_count):
                    m.next = 'Done'
                with m.Else():
                    m.d.sync += triangle_index.eq(triangle_index + 1)
                    m.d.sync += command_read_port.addr.eq(command_read_port.addr + 1)
                    m.next = 'Load Color'
            with m.State('Done'):
                m.d.comb += self.done.eq(1)

        return m


class VertexOrderer(Elaboratable):
    def __init__(self):
        # Inputs
        self.input = Signal(ArrayLayout(ArrayLayout(SCREEN_COORDINATE_SHAPE, 2), 3))

        # Outputs
        self.output = Signal(ArrayLayout(ArrayLayout(SCREEN_COORDINATE_SHAPE, 2), 3))

    def elaborate(self, _):
        m = Module()

        a_above_b = Signal(1)
        b_above_c = Signal(1)
        c_above_a = Signal(1)

        m.d.comb += [
            a_above_b.eq(self.input[0][1] < self.input[1][1]),
            b_above_c.eq(self.input[1][1] < self.input[2][1]),
            c_above_a.eq(self.input[2][1] < self.input[0][1]),
        ]

        with m.If(a_above_b & ~c_above_a):
            m.d.comb += self.output.eq(self.input)
        with m.Elif(b_above_c & ~a_above_b):
            m.d.comb += [
                self.output[0].eq(self.input[1]),
                self.output[1].eq(self.input[2]),
                self.output[2].eq(self.input[0]),
            ]
        with m.Else():
            m.d.comb += [
                self.output[0].eq(self.input[2]),
                self.output[1].eq(self.input[0]),
                self.output[2].eq(self.input[1]),
            ]

        return m


class EdgeGenerator(Elaboratable):
    """Generates pixel coordinates between two points.

    This will output the coordinates of the centers of pixels which are
    to the right of the line and in the range of y values of the
    endpoints of the line.

    start:        pair of Fractional giving the upper (least y) end of
                  the line
    end:          pair of Fractional giving the lower end of the line
    initialize:   Signal that you assert to initialize the circuit
                  (circuit will not run until init goes back to 0)
    request_next: Signal that you assert when you are ready for the
                  next pixel (may be asserted at the same time as init)

    Returns:
    x, y:  coordinates of pixel centers
    valid: signals that x and y are valid.  Will be held at 1 until you
           assert request_next.
    done:  signals that this is the last pixel (asserted at the same
           time as ‘valid’)
    """

    def __init__(self):
        # Inputs
        self.initialize = Signal(1)
        self.start = Signal(point_layout(SCREEN_COORDINATE_SHAPE))
        self.end = Signal(point_layout(SCREEN_COORDINATE_SHAPE))
        self.request_next = Signal(1)

        # Outputs
        self.x = Signal(PIXEL_CENTER_COORDINATE_SHAPE)
        self.y = Signal(PIXEL_CENTER_COORDINATE_SHAPE)
        self.valid = Signal(1)
        self.done = Signal(1)

    def elaborate(self, _):
        m = Module()

        delta_x = self.end.x - self.start.x
        
        first_quadrant_generator = FirstQuadrantEdgeGenerator()
        m.submodules.first_quadrant_generator = first_quadrant_generator
        m.d.comb += [
            first_quadrant_generator.initialize.eq(self.initialize),
            first_quadrant_generator.request_next.eq(self.request_next),
            first_quadrant_generator.delta_x.eq(abs(delta_x)),
            self.y.eq(first_quadrant_generator.y),
            self.valid.eq(first_quadrant_generator.valid),
            self.done.eq(first_quadrant_generator.done),
        ]

        with m.If(delta_x.sign):
            m.d.comb += [
                first_quadrant_generator.start.x.eq(-self.start.x),
                first_quadrant_generator.end.y.eq(self.end.y),
                first_quadrant_generator.end.x.eq(-self.end.x),
                first_quadrant_generator.start.y.eq(self.start.y),
                first_quadrant_generator.include_line.eq(0),
                self.x.eq(Fractional.one() - first_quadrant_generator.x)
            ]
        with m.Else():
            m.d.comb += [
                first_quadrant_generator.start.x.eq(self.start.x),
                first_quadrant_generator.start.y.eq(self.start.y),
                first_quadrant_generator.end.x.eq(self.end.x),
                first_quadrant_generator.end.y.eq(self.end.y),
                first_quadrant_generator.include_line.eq(1),
                self.x.eq(first_quadrant_generator.x)
            ]

        return m


class FirstQuadrantEdgeGenerator(Elaboratable):
    r"""Like EdgeGenerator, but requires that the line doesn’t go left.

    Extra arguments:
    include_line: Signal that tells whether to include pixels that are
                  exactly on the line
                ___     ___     ___     ___     ___     ___     ___     ___            ___     ___     ___     ___ 
    in  clock _/   \___/   \___/   \___/   \___/   \___/   \___/   \___/   \___/ ... _/   \___/   \___/   \___/   \___/
                _______                                                                        _______
    in  init  _/       \________________________________________________________ ... _________/       \________________
              _ ________________________________________________________________     _________ ________________________
    in  start _X_line_0_start___________________________________________________ ... _________X_line_1_start___________
                        _______         _______________________________________                         ______________ 
    in  rqst  _________/       \_______/                                       \ ... __________________/              \
                                _______         _______         _______________      _________                 ________
    out valid _________________/       \_______/       \_______/               \ ...          \_______________/
              _________________ _______________ _______ _______ _______ _______      _ _______ _______________ _______ 
    out x     _///////////////_X_pixl0_________X_pixl1_X_/////_X_pixl2_X_pixl3_X ... _X_pixlN_X_/////////////_X_pixl0_X
                                                                                               _______
        done  __________________________________________________________________ ... _________/       \________________

    While ‘initialize’ is asserted, ‘start’ and ‘end’ must be valid and must
    remain valid until ‘done’ is asserted. request_next may be asserted the
    next cycle after ‘initialize’ and must be held at 1 until ‘done’ is
    asserted. x and y will be valid when ‘valid’ is asserted. ‘done’ is
    asserted and ‘valid’ is deasserted after request_next is asserted and there
    are no more pixels to output. ‘inititalize’ may be asserted on the same
    cylce ‘done’ is asserted to start the next line.

    The algorithm is a modified Bresenham algorithm so we can avoid
    using a divider.  Let the endpoints be ‘start’ and ‘end’, with
    start.y <= end.y.  The ideal line consists of solutions to
        error(x, y) := (x - start.x) Δy - (y - start.y) Δx = 0,
    where Δx = start.x - end.x and Δy = start.y - end.y.  Note that
    ‘error’ is positive in the region that is to the right of and/or
    above the line.

    Let the output coordinates be xᵢ and yᵢ, where ‘i’ is the step
    number (these correspond to the values of self.x and self.y every
    time ‘valid’ is asserted).  These are the coordinates of the pixel
    *centers*.

    Although in the definition of ‘error’ we used x and y as the
    coordinates of an arbitrary point, let us now use them to mean the
    coordinates of the center of the “current” pixel. We want to find
    the first (least x) pixel center on the next row that is to the
    right of the ideal line, i.e. has positive ‘error’. Since the
    current pixel is known to be immediately to the right of the line,
    there are three cases, shown below (in the diagrams, “+” denotes an
    ordinary pixel center and “O” denotes the center of the current
    pixel).
                        _____________
    Case A:             |     |     |
    error(x, y+1) > 0   \  O  |  +  |
    xᵢ₊₁ = x            ||____|_____|
    yᵢ₊₁ = y + 1        ||    |     |
                        | \+  |  +  |
                        |__|__|_____|
                        _____________
    Case B:             |\    |     |
    error(x+1, y+1) > 0 | \O  |  +  |
    and                 |__\__|_____|
    error(x, y+1) <= 0  |   \ |     |
    xᵢ₊₁ = x + 1        |  + \|  +  |
    yᵢ₊₁ = y + 1        |_____\_____|

                        _____________
    Case C:             ˎ     |     |
    error(x+1, y+1) < 0 |ˋ˴O  |  +  |
                        |___ˋ˴ˎ_____|
    Need to look        |     |ˋ˴ˎ  |
    farther to the      |  +  |  +ˋ˴ˎ  
    right (x++).        |_____|_____|

    In cases A and B, we have found the next pixel to output.  In case
    C, we increment x (moving hte current pixel to the right) and try
    again.

    If the ideal line is completely horizontal, or never reaches the
    next pixel center y value before going past end.x, then we could
    stay in case C forever.  To avoid this, we stop if x ≥ end.x.

    We store error(x+1, y+1) in a register.  When x is incremented, we
    just add Δy to the register, and when y is incremented, we subtract
    Δx.  This avoids recomputing the error.  We can get error(x, y+1)
    by subtracting Δy from the register.

    To get the algorithm started, we identify the closest pixel below
    and left of the starting point.  We do not know whether this is the
    first pixel to output, so we set the current pixel to the one just
    above it.  Then the steps above will do the right thing to produce
    x₀, y₀.  We do, however, have to compute the error of the pixel
    x+1, y+1 using the formula.
        init_x = ⌈start.x - 1/2⌉ + 1/2 = 1/2 - ⌊1/2 - start.x⌋
        init_x - start.x = (1/2 - start.x) mod 1
        init_y = ⌈start.y - 1/2⌉ - 1/2 = -1/2 - ⌊1/2 - start.y⌋
        init_y - start.y = (1/2 - start.y) mod 1 - 1
        init_error_below_right = (init_x - start.x + 1) Δy - (init_y - start.y + 1) Δx
    """
    def __init__(self):
        # Inputs
        self.initialize = Signal(1)
        self.start = Signal(point_layout(SCREEN_COORDINATE_SHAPE))
        self.end = Signal(point_layout(SCREEN_COORDINATE_SHAPE))
        self.request_next = Signal(1)
        self.include_line = Signal(1)
        self.delta_x = Signal(SCREEN_COORDINATE_DELTA_SHAPE)

        # Outputs
        self.x = Signal(PIXEL_CENTER_COORDINATE_SHAPE)
        self.y = Signal(PIXEL_CENTER_COORDINATE_SHAPE)
        self.valid = Signal(1)
        self.done = Signal(1, reset=1)

    def elaborate(self, _):
        m = Module()

        state = EdgeGeneratorState()
        m.submodules.state = state
        m.d.comb += state.initialize.eq(self.initialize)
        m.d.comb += state.start_point.eq(self.start)
        m.d.comb += state.delta.x.eq(self.end.x - self.start.x)
        m.d.comb += state.delta.y.eq(self.end.y - self.start.y)
        m.d.comb += self.x.eq(state.x)
        m.d.comb += self.y.eq(state.y)

        with m.If(self.initialize):
            # We are (re)initializing, so we can’t be done and the
            # output isn’t valid.
            m.d.sync += self.valid.eq(0)
            m.d.sync += self.done.eq(0)
        with m.Elif(self.request_next):
            with m.If(self.y + one() < self.end.y):
                with m.If(
                        state.error_below_right.sign
                        | (state.error_below_right == zero()) & ~self.include_line
                ):
                    # Case C: we need to continue searching to the
                    # right.
                    m.d.sync += self.valid.eq(0)
                    m.d.comb += state.increment.x.eq(1)
                with m.Else():
                    # Case A or B: we have found the next pixel to
                    # output.
                    m.d.sync += self.valid.eq(1)
                    m.d.comb += state.increment.y.eq(1)
                    with m.If(
                            state.error_below.sign
                            | (state.error_below == zero()) & ~self.include_line
                    ):
                        # Case B: the next pixel is below and to the
                        # right.
                        m.d.comb += state.increment.x.eq(1)
                    with m.Else():
                        # Case A: the next pixel is below.
                        pass
            with m.Else():
                m.d.sync += self.valid.eq(0)
                m.d.sync += self.done.eq(1)
        with m.Else():
            m.d.sync += self.done.eq(0)

        return m


class EdgeGeneratorState(Elaboratable):
    def __init__(self):
        # Inputs
        self.initialize = Signal(1)
        self.start_point = Signal(point_layout(SCREEN_COORDINATE_SHAPE))
        self.delta = Signal(point_layout(SCREEN_COORDINATE_DELTA_SHAPE))
        self.increment = Signal(point_layout(1))

        # Outputs
        self.x = Signal(PIXEL_CENTER_COORDINATE_SHAPE)
        self.y = Signal(PIXEL_CENTER_COORDINATE_SHAPE)
        self.error_below_right = Signal(EDGE_GENERATOR_ERROR_SHAPE)
        self.error_below = Signal(EDGE_GENERATOR_ERROR_SHAPE)
        self.error_right = Signal(EDGE_GENERATOR_ERROR_SHAPE)

    def elaborate(self, _):
        m = Module()

        initial_value_calculator = EdgeGeneratorInitialValueCalculator()
        m.submodules.initial_value_calculator = initial_value_calculator
        m.d.comb += initial_value_calculator.start_point.eq(self.start_point)
        m.d.comb += initial_value_calculator.delta.eq(self.delta)

        m.d.comb += self.error_below.eq(self.error_below_right - self.delta.y)
        m.d.comb += self.error_right.eq(self.error_below_right + self.delta.x)

        error_increment_0 = Signal(self.delta.y.shape())
        error_increment_1 = Signal(self.delta.x.shape())

        with m.If(self.initialize):
            m.d.sync += self.x.eq(initial_value_calculator.x)
            m.d.sync += self.y.eq(initial_value_calculator.y)
            m.d.sync += self.error_below_right.eq(initial_value_calculator.error_below_right)
        with m.Else():
            with m.If(self.increment.x):
                m.d.sync += self.x.eq(self.x + one())
                m.d.comb += error_increment_0.eq(self.delta.y)

            with m.If(self.increment.y):
                m.d.sync += self.y.eq(self.y + one())
                m.d.comb += error_increment_1.eq(-self.delta.x)

            m.d.sync += \
                self.error_below_right.eq(
                    self.error_below_right + (error_increment_0 + error_increment_1)
                )

        return m


class EdgeGeneratorInitialValueCalculator(Elaboratable):
    def __init__(self):
        # Inputs
        self.start_point = Signal(point_layout(SCREEN_COORDINATE_SHAPE))
        self.delta = Signal(point_layout(SCREEN_COORDINATE_DELTA_SHAPE))

        # Outputs
        self.x = Signal(PIXEL_CENTER_COORDINATE_SHAPE)
        self.y = Signal(PIXEL_CENTER_COORDINATE_SHAPE)
        self.error_below_right = Signal(EDGE_GENERATOR_ERROR_SHAPE)

    def elaborate(self, _):
        m = Module()

        init_x_minus_start_x = Signal(FractionalLayout(False, 0, len(self.start_point.x.fraction)))
        m.d.comb += init_x_minus_start_x.eq((half() - self.start_point.x).mod1())
        m.d.comb += \
            self.x.eq((init_x_minus_start_x + self.start_point.x).truncate_to(fraction_width=1))

        init_y_minus_start_y = Signal(FractionalLayout(True, 2, len(self.start_point.y.fraction)))
        m.d.comb += init_y_minus_start_y.eq((half() - self.start_point.y).mod1() - one())
        m.d.comb += \
            self.y.eq((init_y_minus_start_y + self.start_point.y).truncate_to(fraction_width=1))

        m.d.comb += \
            self.error_below_right.eq(
                (init_x_minus_start_x + one()) * self.delta.y
                - (init_y_minus_start_y + one()) * self.delta.x
            )

        return m


def point_layout(shape=1):
    return StructLayout({'x': shape, 'y': shape})


if __name__ == '__main__':
    import matplotlib.pyplot as plt
    import numpy as np

    from amaranth.sim import *  # type: ignore


    system = EdgeGenerator()

    def bench():
        data = [
            # start_x, start_y, end_x, end_y, init, request_next, x, y, valid, done
            [0x023_A, 0x009_A, 0x028_8, 0x00E_8, 1, 0,     '?',     '?', 0, 0],
            [0x023_A, 0x009_A, 0x028_8, 0x00E_8, 0, 1,     '?',     '?', 0, 0],
            [0x023_A, 0x009_A, 0x028_8, 0x00E_8, 0, 1, 0x024_8, 0x00A_8, 1, 0],
            [0x023_A, 0x009_A, 0x028_8, 0x00E_8, 0, 0, 0x025_8, 0x00B_8, 1, 0],
            [0x023_A, 0x009_A, 0x028_8, 0x00E_8, 0, 0, 0x025_8, 0x00B_8, 1, 0],
            [0x023_A, 0x009_A, 0x028_8, 0x00E_8, 0, 1, 0x025_8, 0x00B_8, 1, 0],
            [0x023_A, 0x009_A, 0x028_8, 0x00E_8, 0, 1, 0x026_8, 0x00C_8, 1, 0],
            [0x023_A, 0x009_A, 0x028_8, 0x00E_8, 0, 0, 0x027_8, 0x00D_8, 1, 0],
            [0x023_A, 0x009_A, 0x028_8, 0x00E_8, 0, 1, 0x027_8, 0x00D_8, 1, 0],
            [0x201_7,-0x0FF_9, 0x322_7, 0x142_7, 1, 0,     '?',     '?', 0, 1],
            [0x201_7,-0x0FF_9, 0x322_7, 0x142_7, 0, 1,     '?',     '?', 0, 0],
            [0x201_7,-0x0FF_9, 0x322_7, 0x142_7, 0, 1, 0x201_8,-0x0FF_8, 1, 0],
            [0x201_7,-0x0FF_9, 0x322_7, 0x142_7, 0, 1, 0x202_8,-0x0FE_8, 1, 0],
            [0x201_7,-0x0FF_9, 0x322_7, 0x142_7, 0, 1, 0x202_8,-0x0FD_8, 1, 0],
            [0x201_7,-0x0FF_9, 0x322_7, 0x142_7, 0, 1, 0x203_8,-0x0FC_8, 1, 0],
            [0x201_7,-0x0FF_9, 0x322_7, 0x142_7, 0, 1, 0x203_8,-0x0FB_8, 1, 0],
            [0x201_7,-0x0FF_9, 0x322_7, 0x142_7, 0, 1, 0x204_8,-0x0FA_8, 1, 0],
            [0x0D2_4, 0x070_8, 0x0DA_4, 0x074_8, 1, 0, 0x204_8,-0x0F9_8, 1, 0],
            [0x0D2_4, 0x070_8, 0x0DA_4, 0x074_8, 0, 1,     '?',     '?', 0, 0],
            [0x0D2_4, 0x070_8, 0x0DA_4, 0x074_8, 0, 1, 0x0D2_8, 0x070_8, 1, 0],
            [0x0D2_4, 0x070_8, 0x0DA_4, 0x074_8, 0, 1,     '?',     '?', 0, 0],
            [0x0D2_4, 0x070_8, 0x0DA_4, 0x074_8, 0, 1, 0x0D4_8, 0x071_8, 1, 0],
            [0x0D2_4, 0x070_8, 0x0DA_4, 0x074_8, 0, 1,     '?',     '?', 0, 0],
            [0x0D2_4, 0x070_8, 0x0DA_4, 0x074_8, 0, 0, 0x0D6_8, 0x072_8, 1, 0],
            [0x0D2_4, 0x070_8, 0x0DA_4, 0x074_8, 0, 1, 0x0D6_8, 0x072_8, 1, 0],
            [0x0D2_4, 0x070_8, 0x0DA_4, 0x074_8, 0, 1,     '?',     '?', 0, 0],
            [0x0D2_4, 0x070_8, 0x0DA_4, 0x074_8, 0, 0, 0x0D8_8, 0x073_8, 1, 0],
            [0x0D2_4, 0x070_8, 0x0DA_4, 0x074_8, 0, 1, 0x0D8_8, 0x073_8, 1, 0],
            [0x020_8, 0x095_8, 0x01E_8, 0x099_8, 1, 0,     '?',     '?', 0, 1],
            [0x020_8, 0x095_8, 0x01E_8, 0x099_8, 0, 1,     '?',     '?', 0, 0],
            [0x020_8, 0x095_8, 0x01E_8, 0x099_8, 0, 1, 0x020_8, 0x095_8, 1, 0],
            [0x020_8, 0x095_8, 0x01E_8, 0x099_8, 0, 1, 0x020_8, 0x096_8, 1, 0],
            [0x020_8, 0x095_8, 0x01E_8, 0x099_8, 0, 1, 0x01F_8, 0x097_8, 1, 0],
            [0x020_8, 0x095_8, 0x01E_8, 0x099_8, 0, 1, 0x01F_8, 0x098_8, 1, 0],
            [0x020_8, 0x095_8, 0x01E_8, 0x099_8, 0, 1,     '?',     '?', 0, 1],
            [0x102_0, 0x066_0, 0x114_0, 0x06A_0, 1, 0,     '?',     '?', 0, 1],
            [0x102_0, 0x066_0, 0x114_0, 0x06A_0, 0, 1,     '?',     '?', 0, 0],
            [0x102_0, 0x066_0, 0x114_0, 0x06A_0, 0, 1,     '?',     '?', 0, 0],
            [0x102_0, 0x066_0, 0x114_0, 0x06A_0, 0, 1, 0x104_8, 0x066_8, 1, 0],
            [0x102_0, 0x066_0, 0x114_0, 0x06A_0, 0, 1,     '?',     '?', 0, 0],
            [0x102_0, 0x066_0, 0x114_0, 0x06A_0, 0, 1,     '?',     '?', 0, 0],
            [0x102_0, 0x066_0, 0x114_0, 0x06A_0, 0, 1,     '?',     '?', 0, 0],
            [0x102_0, 0x066_0, 0x114_0, 0x06A_0, 0, 1,     '?',     '?', 0, 0],
            [0x102_0, 0x066_0, 0x114_0, 0x06A_0, 0, 1, 0x109_8, 0x067_8, 1, 0],
            [0x102_0, 0x066_0, 0x114_0, 0x06A_0, 0, 1,     '?',     '?', 0, 0],
            [0x102_0, 0x066_0, 0x114_0, 0x06A_0, 0, 1,     '?',     '?', 0, 0],
            [0x102_0, 0x066_0, 0x114_0, 0x06A_0, 0, 1,     '?',     '?', 0, 0],
            [0x102_0, 0x066_0, 0x114_0, 0x06A_0, 0, 1, 0x10D_8, 0x068_8, 1, 0],
            [0x102_0, 0x066_0, 0x114_0, 0x06A_0, 0, 1,     '?',     '?', 0, 0],
            [0x102_0, 0x066_0, 0x114_0, 0x06A_0, 0, 1,     '?',     '?', 0, 0],
            [0x102_0, 0x066_0, 0x114_0, 0x06A_0, 0, 1,     '?',     '?', 0, 0],
            [0x102_0, 0x066_0, 0x114_0, 0x06A_0, 0, 1,     '?',     '?', 0, 0],
            [0x102_0, 0x066_0, 0x114_0, 0x06A_0, 0, 1, 0x112_8, 0x069_8, 1, 0],
            [0x055_0, 0x071_4, 0x075_0, 0x071_4, 1, 0,     '?',     '?', 0, 1],
            [0x055_0, 0x071_4, 0x075_0, 0x071_4, 0, 1,     '?',     '?', 0, 0],
            [0x055_0, 0x071_4, 0x075_0, 0x071_4, 0, 0,     '?',     '?', 0, 1],
            [0x055_0, 0x071_8, 0x075_0, 0x071_8, 1, 0,     '?',     '?', 0, 0],
            [0x055_0, 0x071_8, 0x075_0, 0x071_8, 0, 1,     '?',     '?', 0, 0],
            [0x055_0, 0x071_8, 0x075_0, 0x071_8, 0, 1,     '?',     '?', 0, 1],
        ]
        for start_x, start_y, end_x, end_y, initialize, request_next, x, y, valid, done in data:
            yield system.start.x.eq(start_x)
            yield system.start.y.eq(start_y)
            yield system.end.x.eq(end_x)
            yield system.end.y.eq(end_y)
            yield system.initialize.eq(initialize)
            yield system.request_next.eq(request_next)
            yield Settle()
            actual_x = yield system.x.as_value()
            actual_y = yield system.y.as_value()
            actual_valid = yield system.valid
            actual_done = yield system.done
            assert x == '?' or actual_x << 3 == x
            assert y == '?' or actual_y << 3 == y
            assert actual_valid == valid
            assert actual_done == done
            yield

    simulator = Simulator(system)
    simulator.add_clock(1e-6)
    simulator.add_sync_process(bench)
    with simulator.write_vcd('gpu_edge_generator.vcd'):
        simulator.run()

    def strings_to_array(strings):
        height = len(strings)
        width = len(strings[0])
        array = np.empty((height, width), dtype=bool)
        for y, string in enumerate(strings):
            for x, char in enumerate(string):
                if char == '.':
                    array[y,x] = False
                elif char == '#':
                    array[y,x] = True
                else:
                    raise ValueError(f'Unexpectected character {char!r}')
        return array

    def bool_array_diff(a, b):
        diff = (a & b).astype(np.uint8)
        diff |= 2 * (a & ~b).astype(np.uint8)
        diff |= 3 * (b & ~a).astype(np.uint8)
        return diff

    def array_diff_to_string(diff):
        return '\n'.join(''.join('.#-+'[i] for i in row) for row in diff)

    def test_gpu(vertices, expected_image, plot=False):
        height = len(expected_image)
        width = len(expected_image[0])
        expected_binary_image = strings_to_array(expected_image)
        actual_image = np.empty((height, width), dtype=bool)
        command_buffer_init = [1, 0xFFF, *(comp for vertex in vertices for comp in vertex)]
        system = Gpu(width, height, component_width=8, command_buffer_init=command_buffer_init)

        def bench():
            while not (yield system.done):
                yield

            for y in range(height):
                for x in range(width):
                    color = yield system.framebuffer[width*y+x]
                    actual_image[y][x] = color != 0

        simulator = Simulator(system)
        simulator.add_clock(1e-6)
        simulator.add_sync_process(bench)
        simulator.run()

        if not np.array_equal(expected_binary_image, actual_image):
            print(
                '['
                +
                ', '.join('[' + ', '.join(f'{c:04X}' for c in v) + ']' for v in vertices)
                +
                ']'
            )
            print(array_diff_to_string(bool_array_diff(expected_binary_image, actual_image)))
        if plot:
            image = actual_image[:,:,np.newaxis] * np.array([[[0xFF, 0xFF, 0xFF]]], dtype=np.uint8)
            _, ax = plt.subplots()
            ax.imshow(image, alpha=0.5, extent=(0, width, height, 0))
            points = [[command_buffer_init[2*n+i] / 0x100 for i in range(2)] for n in range(3)]
            ax.plot(
                [points[0][0], points[1][0], points[2][0], points[0][0]],
                [points[0][1], points[1][1], points[2][1], points[0][1]],
            )
            plt.show()

    test_gpu(
        [[0x00B_0, 0x002_0], [0x005_0, 0x008_0], [0x00B_0, 0x00E_0]],
        [
            '..................',
            '..................',
            '..........#.......',
            '.........##.......',
            '........###.......',
            '.......####.......',
            '......#####.......',
            '.....######.......',
            '.....######.......',
            '......#####.......',
            '.......####.......',
            '........###.......',
            '.........##.......',
            '..........#.......',
            '..................',
            '..................',
        ],
    )
    test_gpu(
        [[0x007_8, 0x002_0], [0x001_8, 0x00E_0], [0x00B_8, 0x009_0]],
        [
            '................',
            '................',
            '.......#........',
            '.......#........',
            '......###.......',
            '......###.......',
            '.....#####......',
            '.....######.....',
            '....#######.....',
            '....######......',
            '...#####........',
            '...###..........',
            '..##............',
            '................',
            '................',
        ],
    )
    test_gpu(
        [[0x006_0, 0x00A_0], [0x007_0, 0x009_8], [0x006_8, 0x008_F]],
        [
            '................',
            '................',
            '................',
            '................',
            '................',
            '................',
            '................',
            '................',
            '................',
            '......#.........',
            '................',
            '................',
            '................',
            '................',
            '................',
            '................',
        ],
    )
    test_gpu(
        [[0x002_0, 0x002_0], [0x00E_0, 0x010_0], [0x004_0, 0x002_0]],
        [
            '................',
            '................',
            '..##............',
            '...##...........',
            '....##..........',
            '.....#..........',
            '......#.........',
            '.......#........',
            '........#.......',
            '........#.......',
            '.........#......',
            '..........#.....',
            '................',
            '................',
            '................',
            '................',
        ],
    )
    test_gpu(
        [[0x00E_0, 0x00F_2], [0x004_0, 0x001_2], [0x002_0, 0x001_2]],
        [
            '................',
            '..##............',
            '...##...........',
            '....##..........',
            '.....#..........',
            '......#.........',
            '.......#........',
            '.......##.......',
            '........#.......',
            '.........#......',
            '..........#.....',
            '................',
            '................',
            '................',
            '.............#..',
            '................',
        ],
    )

    WIDTH = 80
    HEIGHT = 60
    command_buffer_init = \
        [
            2,
            0xF79,
            0x021_0, 0x002_0,
            0x011_0, 0x01A_0,
            0x042_0, 0x02E_0,
            0x3E2,
            0x025_0, 0x03B_0,
            0x043_0, 0x01C_0,
            0x017_0, 0x022_0,
        ]
    system = Gpu(WIDTH, HEIGHT, component_width=4, command_buffer_init=command_buffer_init)
    image = np.empty((HEIGHT, WIDTH, 3), dtype=np.uint8)

    def bench():
        while not (yield system.done):
            yield

        for y in range(HEIGHT):
            for x in range(WIDTH):
                color = yield system.framebuffer[WIDTH*y+x]
                image[y][x][0] = (color & 0xF) * 0x11
                image[y][x][1] = (color >> 4 & 0xF) * 0x11
                image[y][x][2] = (color >> 8 & 0xF) * 0x11

    simulator = Simulator(system)
    simulator.add_clock(1e-6)
    simulator.add_sync_process(bench)
    with simulator.write_vcd('gpu_rasterizer.vcd'):
        simulator.run()
    simulator.run()

    fix, ax = plt.subplots()
    ax.imshow(image, alpha=0.5, extent=(0, WIDTH, HEIGHT, 0))
    points = [[command_buffer_init[2*n+i + 2] / 0x10 for i in range(2)] for n in range(3)]
    ax.plot(
        [points[0][0], points[1][0], points[2][0], points[0][0]],
        [points[0][1], points[1][1], points[2][1], points[0][1]],
    )
    plt.show()
