from amaranth import *  # type: ignore


class ClockGenerator(Elaboratable):
    def __init__(self):
        # Input
        self.clock_in_25MHz = Signal()

        # Outputs
        self.clock_250MHz = Signal()
        self.locked = Signal()

    def elaborate(self, _):
        m = Module()

        feedback_clock = Signal()

        # Based on the ocppll code in prjtrellis, CPHASE is a phase
        # shift in multiples of the VCO period, and FPHASE is an
        # additional phase shift in multiples of 1/8 the VCO period.
        # (This differs from how these parameters are used for dynamic
        # phase shifting, apparently.)
        m.submodules.pll = \
            Instance(
                'EHXPLLL',
                a_ICP_CURRENT='9',
                a_LPF_RESISTOR='8',
                a_MFG_ENABLE_FILTEROPAMP='1',
                a_MFG_GMCREF_SEL='2',
                p_PLLRST_ENA='DISABLED',
                p_INTFB_WAKE='DISABLED',
                p_STDBY_ENABLE='DISABLED',
                p_DPHASE_SOURCE='DISABLED',
                p_CLKOP_FPHASE=0,
                p_CLKOS_FPHASE=0,
                p_CLKOP_CPHASE=1,  # Static phase shift by 1 VCO period = 1/2 * 360º = 180º
                p_OUTDIVIDER_MUXA='DIVA',
                p_OUTDIVIDER_MUXB='DIVB',
                p_OUTDIVIDER_MUXC='DIVC',
                p_OUTDIVIDER_MUXD='DIVD',
                p_CLKOP_ENABLE='ENABLED',
                p_CLKOS_ENABLE='DISABLED',
                p_CLKOS2_ENABLE='DISABLED',
                p_CLKOS3_ENABLE='DISABLED',
                p_CLKOP_DIV=2,
                p_CLKFB_DIV=10,
                p_CLKI_DIV=1,
                p_FEEDBK_PATH='INT_OP',
                i_CLKI=self.clock_in_25MHz,
                i_CLKFB=feedback_clock,
                i_PHASESEL0=0,
                i_PHASESEL1=0,
                i_PHASEDIR=0,
                i_PHASESTEP=0,
                i_PHASELOADREG=0,
                i_STDBY=0,
                i_PLLWAKESYNC=0,
                i_RST=0,
                i_ENCLKOP=0,
                i_ENCLKOS=0,
                i_ENCLKOS2=0,
                i_ENCLKOS3=0,
                o_CLKOP=self.clock_250MHz,
                o_CLKINTFB=feedback_clock,
                o_INTLOCK=self.locked,
            )

        return m
