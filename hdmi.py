from amaranth import *  # type: ignore
from amaranth.lib.data import *  # type: ignore
from amaranth.lib.cdc import PulseSynchronizer

from tmds import TmdsEncoder


# This is pretending we have a pixel clock of 25.175 MHz
SCANLINE_COLUMNS = 800
VISIBLE_COLUMNS = 640
HSYNC_START_COLUMN = 656
HSYNC_END_COLUMN = 752
FRAME_ROWS = 525
VISIBLE_ROWS = 480
VSYNC_START_ROW = 490
VSYNC_END_ROW = 492
VISIBLE_PIXELS = VISIBLE_COLUMNS * VISIBLE_ROWS


class HdmiOutput(Elaboratable):
    def __init__(self):
        # Inputs
        self.color_in = Signal(ArrayLayout(8, 3))

        # Outputs
        self.ready = Signal()  # True when we can accept pixel data
        self.new_line = Signal()  # True on the cycle before the start of a new line
        self.new_frame = Signal()  # True on the cycle before the start of a new frame
        self.color_out = Signal(3)

    def elaborate(self, _):
        m = Module()

        row = Signal(range(FRAME_ROWS))
        column = Signal(range(SCANLINE_COLUMNS))

        with m.If(column == SCANLINE_COLUMNS - 1):
            m.d.sync += column.eq(0)
        with m.Else():
            m.d.sync += column.eq(column + 1)

        with m.If(column == SCANLINE_COLUMNS - 1):
            with m.If(row == FRAME_ROWS - 1):
                m.d.sync += row.eq(0)
            with m.Else():
                m.d.sync += row.eq(row + 1)

        m.d.comb += self.new_line.eq(column == SCANLINE_COLUMNS - 1)
        m.d.comb += self.new_frame.eq((column == SCANLINE_COLUMNS - 1) & (row == FRAME_ROWS - 1))

        # Determine when we are in a drawable area
        in_drawable_area = Signal()
        m.d.comb += in_drawable_area.eq((column < VISIBLE_COLUMNS) & (row < VISIBLE_ROWS))
        m.d.comb += self.ready.eq(in_drawable_area)

        # Generate horizontal and vertical sync pulses
        hsync = Signal()
        vsync = Signal()
        with m.Switch(column):
            with m.Case(HSYNC_START_COLUMN - 1):
                m.d.sync += hsync.eq(1)
            with m.Case(HSYNC_END_COLUMN - 1):
                m.d.sync += hsync.eq(0)
        with m.Switch(row):
            with m.Case(VSYNC_START_ROW - 1):
                m.d.sync += vsync.eq(1)
            with m.Case(VSYNC_END_ROW - 1):
                m.d.sync += vsync.eq(0)

        # Convert the 8-bit color values into 10-bit TMDS-encoded values
        tmds_color = Signal(ArrayLayout(10, 3))
        tmds_encoders = [TmdsEncoder() for _ in range(3)]
        for i in range(3):
            m.submodules[f'tmds_encoder{i}'] = tmds_encoders[i]
            m.d.comb += tmds_encoders[i].video_data.eq(self.color_in[i])
            m.d.comb += tmds_encoders[i].video_data_enable.eq(in_drawable_area)
            m.d.comb += tmds_color[i].eq(tmds_encoders[i].encoded_data)
            if i != 2:
                m.d.comb += tmds_encoders[i].control_data.eq(0)
            else:
                m.d.comb += tmds_encoders[i].control_data.eq(Cat(hsync, vsync))

        # Strobe tmds_shift_register_load once every 10 TMDS clock cycles,
        # i.e. at the start of new pixel data
        #tmds_bit_counter = Signal(range(10))
        tmds_shift_register_load = Signal(reset=1)
        #m.d.tmds += tmds_bit_counter.eq(Mux(tmds_bit_counter == 9, 0, tmds_bit_counter + 1))
        #m.d.comb += tmds_shift_register_load.eq(tmds_bit_counter == 0)
        bit_counter_shift_registers = [Signal() for _ in range(9)]
        m.d.tmds += bit_counter_shift_registers[0].eq(tmds_shift_register_load)
        for i in range(1, 9):
            m.d.tmds += bit_counter_shift_registers[i].eq(bit_counter_shift_registers[i-1])
        m.d.tmds += tmds_shift_register_load.eq(bit_counter_shift_registers[-1])

        # Latch the TMDS color values into three shift registers at the
        # start of the pixel, then shift them one bit each TMDS clock.
        # We will then output the LSB on each TMDS clock edge.
        tmds_shift_registers = Signal(ArrayLayout(10, 3))
        with m.If(tmds_shift_register_load):
            m.d.tmds += tmds_shift_registers.eq(tmds_color)
        with m.Else():
            for i in range(3):
                m.d.tmds += tmds_shift_registers[i].eq(tmds_shift_registers[i].shift_right(1))

        # Finally output the LSB of each color shift register
        for i in range(3):
            m.d.comb += self.color_out[i].eq(tmds_shift_registers[i][0])

        return m


if __name__ == '__main__':
    from amaranth.sim import Simulator

    system = Module()
    system.submodules.hdmi = hdmi = HdmiOutput()

    column = Signal(range(640))
    with system.If(hdmi.new_line):
        system.d.sync += column.eq(0)
    with system.Else():
        system.d.sync += column.eq(column + 1)

    system.d.comb += hdmi.color_in.eq(column)

    simulator = Simulator(system)
    simulator.add_clock(40e-9, domain='sync')
    simulator.add_clock(4e-9, domain='tmds', phase=2e-9)
    with simulator.write_vcd('hdmi_test.vcd'):
        simulator.run_until(4e-6, run_passive=True)
