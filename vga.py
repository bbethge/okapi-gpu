from math import log2

from amaranth import *  # type: ignore
from amaranth.lib.data import *  # type: ignore


# This is pretending we have a pixel clock of 25.175 MHz
SCANLINE_COLUMNS = 800
VISIBLE_COLUMNS = 640
HSYNC_START_COLUMN = 656
HSYNC_END_COLUMN = 752
FRAME_ROWS = 525
VISIBLE_ROWS = 480
VSYNC_START_ROW = 490
VSYNC_END_ROW = 492
VISIBLE_PIXELS = VISIBLE_COLUMNS * VISIBLE_ROWS
CHANNEL_WIDTH = 4


class Vga(Elaboratable):
    def __init__(self, pixel_size=1):
        assert log2(pixel_size) % 1 == 0

        # Parameters
        self.pixel_size = pixel_size
        self._discard_bits = int(log2(pixel_size))
        self.framebuffer_size = VISIBLE_PIXELS // pixel_size**2

        # Inputs
        self.framebuffer_data = Signal(ArrayLayout(CHANNEL_WIDTH, 3))

        # Outputs
        self.framebuffer_address = Signal(range(self.framebuffer_size))
        self.framebuffer_read_enable = Signal(1, reset=1)
        self.red = Signal(CHANNEL_WIDTH)
        self.green = Signal(CHANNEL_WIDTH)
        self.blue = Signal(CHANNEL_WIDTH)
        self.hsync = Signal(1, reset=1)
        self.vsync = Signal(1, reset=1)

    def elaborate(self, _):
        m = Module()

        row = Signal(range(FRAME_ROWS))
        column = Signal(range(SCANLINE_COLUMNS))

        column_will_be_visible = Signal(1, reset=1)
        row_will_be_visible = Signal(1, reset=1)
        m.d.comb += self.framebuffer_read_enable.eq(column_will_be_visible & row_will_be_visible)
        visible = Signal(1)
        m.d.sync += visible.eq(self.framebuffer_read_enable)

        # Since the framebuffer is synchronous memory, the pixel is output 1
        # cycle after its coordinates are in ‘row’ and ‘column’.

        with m.If(column == SCANLINE_COLUMNS - 1):
            m.d.sync += column.eq(0)
            with m.If(row == FRAME_ROWS - 1):
                m.d.sync += row.eq(0)
            with m.Else():
                m.d.sync += row.eq(row + 1)
        with m.Else():
            m.d.sync += column.eq(column + 1)

        with m.Switch(column):
            with m.Case(0):
                with m.Switch(row):
                    with m.Case(VSYNC_START_ROW):
                        m.d.sync += self.vsync.eq(0)
                    with m.Case(VSYNC_END_ROW):
                        m.d.sync += self.vsync.eq(1)
            with m.Case(VISIBLE_COLUMNS - 1):
                m.d.sync += column_will_be_visible.eq(0)
            with m.Case(HSYNC_START_COLUMN):
                m.d.sync += self.hsync.eq(0)
            with m.Case(HSYNC_END_COLUMN):
                m.d.sync += self.hsync.eq(1)
            with m.Case(SCANLINE_COLUMNS - 1):
                m.d.sync += column_will_be_visible.eq(1)
                with m.Switch(row):
                    with m.Case(FRAME_ROWS - 1):
                        m.d.sync += row_will_be_visible.eq(1)
                    with m.Case(VISIBLE_ROWS - 1):
                        m.d.sync += row_will_be_visible.eq(0)

        with m.If(column == SCANLINE_COLUMNS - 1):
            with m.If(row == FRAME_ROWS - 1):
                m.d.sync += self.framebuffer_address.eq(0)
            with m.Elif(row_will_be_visible):
                with m.If(~row[:self._discard_bits].all()):
                    m.d.sync += \
                        self.framebuffer_address.eq(
                            self.framebuffer_address - VISIBLE_COLUMNS//self.pixel_size
                        )
        with m.Elif(visible):
            with m.If(column[:self._discard_bits].all()):
                m.d.sync += self.framebuffer_address.eq(self.framebuffer_address + 1)

        m.d.comb += Cat(self.red, self.green, self.blue).eq(Mux(visible, self.framebuffer_data, 0))

        return m


if __name__ == '__main__':
    from amaranth.sim import Simulator


    system = Module()
    vga = Vga(pixel_size=4)
    system.submodules.vga = vga
    framebuffer = Memory(width=12, depth=160*120, init=list(range(160))*120)
    system.submodules.framebuffer = framebuffer
    framebuffer_read_port = framebuffer.read_port()
    system.d.comb += framebuffer_read_port.en.eq(vga.framebuffer_read_enable)
    system.d.comb += framebuffer_read_port.addr.eq(vga.framebuffer_address)
    system.d.comb += vga.framebuffer_data.eq(framebuffer_read_port.data)
    simulator = Simulator(system)
    simulator.add_clock(1 / 25e6)
    with simulator.write_vcd('vga_test.vcd'):
        simulator.run_until(2 / 60, run_passive=True)
