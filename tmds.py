from functools import reduce
import operator as op

from amaranth import *  # type: ignore


class TmdsEncoder(Elaboratable):
    def __init__(self):
        # Inputs
        self.video_data = Signal(8)
        self.control_data = Signal(2)
        self.video_data_enable = Signal()

        # Output
        self.encoded_data = Signal(10)

    def elaborate(self, _):
        m = Module()

        video_data_ones_count = Signal(4)
        m.d.comb += video_data_ones_count.eq(reduce(op.add, self.video_data))

        should_use_xnor = Signal()
        # This is a magic way of calculating which encoding (xor or
        # xnor) will have fewer transitions.
        m.d.comb += \
            should_use_xnor.eq(
                (video_data_ones_count > 4)
                | (video_data_ones_count == 4) & (self.video_data[0] == 0)
            )

        # This is the result of the first stage, an X(N)OR-encoded byte
        # plus a bit to tell which encoding was used.
        tm_encoded_video_data = Signal(9)
        tm_encoded_bits = [Signal() for _ in range(9)]
        m.d.comb += tm_encoded_bits[0].eq(self.video_data[0])
        for i in range(1, 8):
            m.d.comb += \
                tm_encoded_bits[i].eq(tm_encoded_bits[i-1] ^ self.video_data[i] ^ should_use_xnor)
        m.d.comb += tm_encoded_bits[8].eq(~should_use_xnor)
        m.d.comb += tm_encoded_video_data.eq(Cat(*tm_encoded_bits))

        balance_accumulator = Signal(signed(4))
        balance = Signal(signed(4))
        m.d.comb += balance.eq(reduce(op.add, tm_encoded_video_data[:8]) - 4)
        balance_signs_equal = Signal()
        m.d.comb += balance_signs_equal.eq(balance[-1] == balance_accumulator[-1])

        should_invert = Signal()
        with m.If((balance == 0) | (balance_accumulator == 0)):
            m.d.comb += should_invert.eq(~tm_encoded_video_data[8])
        with m.Else():
            m.d.comb += should_invert.eq(balance_signs_equal)

        balance_accumulator_increment = Signal(4)
        # What is happening here?
        m.d.comb += \
            balance_accumulator_increment.eq(
                balance - (
                    (tm_encoded_video_data[8] ^ ~balance_signs_equal)
                    & ~((balance == 0) | (balance_accumulator == 0))
                )
            )

        new_balance_accumulator = Signal(4)
        with m.If(should_invert):
            m.d.comb += \
                new_balance_accumulator.eq(balance_accumulator - balance_accumulator_increment)
        with m.Else():
            m.d.comb += \
                new_balance_accumulator.eq(balance_accumulator + balance_accumulator_increment)

        tmds_data = Signal(10)
        m.d.comb += \
            tmds_data.eq(
                Cat(
                    tm_encoded_video_data[:8] ^ should_invert.replicate(8),
                    tm_encoded_video_data[8],
                    should_invert,
                )
            )

        tmds_code = Signal(10)
        with m.Switch(self.control_data):
            with m.Case(0):
                m.d.comb += tmds_code.eq(0b1101010100)
            with m.Case(1):
                m.d.comb += tmds_code.eq(0b0010101011)
            with m.Case(2):
                m.d.comb += tmds_code.eq(0b0101010100)
            with m.Case(3):
                m.d.comb += tmds_code.eq(0b1010101011)

        with m.If(self.video_data_enable):
            m.d.sync += self.encoded_data.eq(tmds_data)
            m.d.sync += balance_accumulator.eq(new_balance_accumulator)
        with m.Else():
            m.d.sync += self.encoded_data.eq(tmds_code)
            m.d.sync += balance_accumulator.eq(0)

        return m
